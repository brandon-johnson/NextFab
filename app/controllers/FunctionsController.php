<?php

class FunctionsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Functions Controller
	|--------------------------------------------------------------------------
	|
	| 
	|
	*/

	public function getIndex()
	{
		if(Auth::check()){
			if(Auth::user()->is_admin == 2){
				return View::make('clientdash');
			}
			else{
				return View::make('home');
			}
		}
		else{
			return View::make('home');	
		}
		
	}
	
	public function getMyJobs()
	{
		return View::make('myjobs');
	}
	
	public function getJobBoard()
	{
		return View::make('jobboard');
	}
	
	public function getMemberDirectory()
	{
		return View::make('memberdirectory');
	}
	
	public function getDiscussionBoard()
	{
		return View::make('discussion');
	}
	public function getCreateDiscussion()
	{
		return View::make('creatediscussionpost');
	}
	public function getJobApproval()
	{
		return View::make('approval');
	}
	
	public function getIndividualJob()
	{
		return View::make('individualjob');
	}
	public function getClientDash()
	{
		return View::make('clientdash');
	}
	
	public function getRegister($AUTH_KEY)
	{
		if(Keys::where('auth_key', '=', $AUTH_KEY)->exists())
		{
		   //key found
			foreach(Keys::where('auth_key', '=', $AUTH_KEY)->get() as $auth)
			{
				if($auth->is_void == '1')
				{
					//valid
					return View::make('register')->with('message', '<div class="alert alert-success">Authentication: ' . $AUTH_KEY . ' Key is valid</div>');	
				}
				else
				{
					//invalid
					return Redirect::to('/')->with('message', '<div class="alert alert-danger">Authentication Key is expired.</div>');
				}		
			}
		}
		else
		{
			return Redirect::to('/')->with('message', '<div class="alert alert-danger">Authentication Key does not exist.</div>');
		}
	}
	
	public function postRegister()
	{
		$userData = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'password_confirm' => Input::get('password_confirm'),
			'email' => Input::get('email'),
			'email_confirm' => Input::get('email_confirm')
		);
		
		//create validation rules
		$rules = array(
			'username' => 'Required',
			'password' => 'Required',
			'password_confirm' => 'Required',
			'email' => 'Required',
			'email_confirm' => 'Required'
		);
		
		$validator = Validator::make($userData, $rules);
		if($validator->passes())
		{
			$password = Hash::make($userData['password']);
			//validation passed
			$User = new User();
			$User ->uid = uniqid('nextfab_');
			$User ->username = $userData['username'];
			$User ->email = $userData['email'];
			$User ->email_confirm = $userData['email_confirm'];
			$User ->password = $password;
			$User ->password_confirm = $password;
			$User ->ip = $_SERVER['REMOTE_ADDR'];
			
			$User ->save();
			
			//dont forget to deactivate auth_key used
			
			return Redirect::to('/')->with('message', 'success');
		}
		else
		{
			//validation failed
			return Redirect::to('/')->with('message', 'failure');
		}
	}
	
	public function postLogin()
	{
		$userData = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
		
		$rules = array(
			'username' => 'Required',
			'password' => 'Required'
		);
		
		$validator = Validator::make($userData, $rules);
		if($validator->passes())
		{
			//validation passed
			if (Auth::attempt(array('username' => $userData['username'], 'password' => $userData['password']), true))
			{
				//auth succedded
				return Redirect::to('/')->with('message', '<div class="alert alert-success">Thanks <strong>' . $userData['username'] . '</strong>, you have been successfully authenticated.</div>');
			}
			else
			{
				//auth failed
				return Redirect::to('/')->with('message', '<div class="alert alert-danger">Sorry, you failed to authenticate.</div>');
			}
		}
		else
		{
			//validation failed
			return Redirect::to('/')->with('message', '<div class="alert alert-danger">Sorry, you failed to authenticate.</div>');
		}
	}
	
	public function getLogout()
	{
		Auth::logout();
		
		return Redirect::to('/')->with('message', '<div class="alert alert-info">You have been successfully logged out.</div>');
	}
	
	public function getProfile($username)
	{
		foreach(User::where('username', '=', $username)->get() as $user)
		{
			$userData = array(
				'username' => $user ->username,
				'about' => $user ->about,
				'picture' => $user ->picture_url
			);
		}
		return View::make('account.profile')->with('userData', $userData);
	}
	
	public function getEditProfile()
	{
		if (Auth::check())
		{
			// The user is logged in...
			$userData = array(
			'username' => Auth::user()->username,
			'about' => Auth::user()->about,
			'picture' => Auth::user()->picture_url,
			);
			
			return View::make('account.edit')->with('userData', $userData);
		}
		else{
			
		}
	}
	
	public function postEditProfile()
	{
		if(Auth::check())
		{
			$avi = uniqid('avi_');
			$userData = array(
				'username' => Auth::user()->username,
				'about' => Input::get('about')
			);
			if (Input::hasFile('avatar'))
			{			
				$file = Input::file('avatar');
				$file->move('avatars', $avi);
				
				$User = User::find(Auth::user()->id);
				$User->about = $userData['about'];
				$User->picture_url = 'http://aidevserver.co/projects/nextfab/public/avatars/' . $avi;
				
				$User->save();
				
				return Redirect::to('/')->with('message', '<div class="alert alert-success">Thanks ' . $userData['username'] . ', your profile has been updated.</div>');
			}
			
			else
			{
				$User = User::find(Auth::user()->id);
				$User->about = $userData['about'];
				$User->save();

				return Redirect::to('/')->with('message', '<div class="alert alert-success">Thanks ' . $userData['username'] . ', your profile has been updated.</div>');
			}
		}
		else
		{
			
		}
	}
	
	public function checkUser($view)
	{
		return View::make($view);
	}
	
	public function getViewPost($id)
	{
		return View::make('individual_post')->with('id', $id);
	}
	
	public function postSubmitDiscussion()
	{
		$discussionData = array(
			'title' => Input::get('title'),
			'post' => Input::get('post'),
			'author' => Auth::user()->username,
			'picture_url' => Auth::user()->picture_url,
			'category' => Input::get('category')
		);
		
		$rules = array(
			'title' => 'Required',
			'post' => 'Required',
			'author' => 'Required',
			'picture_url' => 'Required',
			'category' => 'Required'
		);
		
		$validator = Validator::make($discussionData, $rules);
		if($validator->passes())
		{
			$Discussion = new Discussion();
			$Discussion->title = $discussionData['title'];
			$Discussion->post = $discussionData['post'];
			$Discussion->author = $discussionData['author'];
			$Discussion->picture_url = $discussionData['picture_url'];
			$Discussion->category = $discussionData['category'];
			
			$Discussion ->save();
			
			return Redirect::to('/')->with('message', 'success');
		}
		else
		{
			return Redirect::to('/')->with('message', 'failed');
		}
	}
	
	public function getUpvote($id)
	{
		if(Auth::check())
		{
			
		}
		else{
			return Redirect::to('posts/' . $id)->with('message', '<div class="alert alert-danger">User not logged in.</div>');
		}
	}
	
	public function getDownvote($id)
	{
		if(Auth::check()){
			
		}
		else{
			return Redirect::to('posts/' . $id)->with('message', '<div class="alert alert-danger">User not logged in.</div>');	
		}
	}
}