@include('layouts.header')
<!--REGISTRATION PAGE CONTENT-->
<div class="registration-page-container">
	<div class="">
		<section class="container">
			<div class="container-page">
				<div class="col-md-6">
					{{ Form::open(array('url' => 'register/submit')) }}
					<h3 class="dark-grey">Registration</h3>

					<div class="form-group col-lg-12">
						<label>Username</label>
						<br/>
						<input type="text" name="username" class="form-control" id="username" value="">
					</div>

					<div class="form-group col-lg-6">
						<label>Password</label>
						<input type="password" name="password" class="form-control" id="oassword" value="">
					</div>

					<div class="form-group col-lg-6">
						<label>Repeat Password</label>
						<input type="password" name="password_confirm" class="form-control" id="password_confirm" value="">
					</div>

					<div class="form-group col-lg-6">
						<label>Email Address</label>
						<input type="email" name="email" class="form-control" id="email" value="">
					</div>

					<div class="form-group col-lg-6">
						<label>Repeat Email Address</label>
						<input type="email" name="email_confirm" class="form-control" id="email_confirm" value="">
					</div>


				</div>

				<div class="col-md-6">
					<h3 class="dark-grey">Terms and Conditions</h3>
					<p>
						By clicking on "Register" you agree to The Company's' Terms and Conditions
					</p>
					<p>
						While rare, prices are subject to change based on exchange rate fluctuations - should such a fluctuation happen, we may request an additional payment. You have the option to request a full refund or to pay the new price. (Paragraph 13.5.8)
					</p>
					<p>
						Should there be an error in the description or pricing of a product, we will provide you with a full refund (Paragraph 13.5.6)
					</p>
					<p>
						Acceptance of an order by us is dependent on our suppliers ability to provide the product. (Paragraph 13.5.6)
					</p>

					<input type="submit" class="btn btn-primary" value="Register" />
				</div>
			{{ Form::close() }}
			</div>
		</section>
	</div>
</div>
@include('layouts.footer')