@include('layouts.header')

<!--START HOME MEMBER-DASHBOARD CONTENT-->
<div class="wrapper desktop">
	<h2 class="member-dash-title">My Jobs</h2>

	<!-- toggle between current and previous jobs -->
	<div class="btn-group btn-toggle">
		<button id="current-jobs" class="btn btn-sm btn-primary">Current Jobs</button>
		<button id="previous-jobs" class="btn btn-sm btn-default">Previous Jobs</button>
	</div>

	<div class="row member-dash">
		<!-- jobs list : active job: grey, attn job header read with exclamation mark -->
		<div class="col-md-3 jobs-list">
			<!-- individual job -->
			<li class="job">
				<h2 class="job-title">Job Title</h2>
				<a href="#" class="client-email">johndoe@fake.com</a>
				<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
				<button class="btn btn-default view-job">View</button>
			</li>

			<li class="job active-job">
				<!-- notifications -->
				<button class="job-notifications">1</button>
				<!-- mark job as complete or on hold -->

				<ul class="notification-controls">
					<li class="mark-complete"><a href="#">Mark Complete</a>
					</li>
					<li class="hold-job"><a href="#">Hold Job</a>
					</li>
				</ul>

				<h2 class="job-title">Other Job Title</h2>
				<a href="#" class="client-email">johndoe@fake.com</a>
				<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
				<button class="btn btn-default view-job">View</button>
			</li>

			<li class="job">
				<!-- notifications -->
				<button class="job-notifications">1</button>
				<!-- mark job as complete or on hold -->

				<ul class="notification-controls">
					<li class="mark-complete"><a href="#">Mark Complete</a>
					</li>
					<li class="hold-job"><a href="#">Hold Job</a>
					</li>
				</ul>

				<h2 class="job-title job-title-attn">Other Job Title</h2>
				<a href="#" class="client-email">johndoe@fake.com</a>
				<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
				<button class="btn btn-default view-job">View</button>
			</li>
		</div>

		<!-- job post -->
		<div class="col-md-3 job-post">
			<h2 class="column-title"><i>Job Post</i></h2>
			<div class="selected-job-title">
				<h2 class="title">This is an Example Job Title</h2>

				<h4>Required Certifications</h4>
				<div class="skills-widgets-center">
					<div class="skill-qual-left">1</div>
					<div class="skill-qual-left">2</div>
					<div class="skill-qual-left">3</div>
					<div class="skill-qual-left">4</div>
				</div>
			</div>
			<div class="job-description">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
					elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
					amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
					adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
				</p>
			</div>
		</div>
		<!-- client discussion -->
		<div class="col-md-6 client-discussion">
			<h2 class="column-title"><i>Client Discussion</i></h2>
			<!-- converstaion section  -->
			<div class="convo-top">
				<!-- user message to the left -->
				<div class="user-message">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>
					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
				</div>

				<!-- member reply to the right -->
				<div class="member-reply">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>

					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
				</div>

				<div class="member-reply">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>

					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
				</div>
				<div class="user-message">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>
					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
				</div>
				<div class="member-reply">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>

					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
				</div>
				<div class="user-message">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>
					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
				</div>

			</div>

			<!-- reply section -->
			<div class="reply-section">
				<textarea class="reply-text" name="reply-text" cols="30" rows="5"></textarea>
				<div class="reply-bottom">

					<label class="btn btn-default add-files">
						<input type="file" required/>
						<span><i class="fa fa-paperclip"></i> Add Files</span>
					</label>

					<button class="btn btn-default send">Send</button>
				</div>
			</div>
		</div>
		<!--       end of client discussion
          start of client rating -->
		<div class="col-md-6 client-rating">
			<h2 class="column-title"><i>Client Rating</i></h2>
			<!-- converstaion section  -->
			<div class="review-middle">
				<!-- category 1 -->
				<div class="rating-inline category-rating">
					<h3> Category </h3>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span> 
				</div>

				<!-- category 2 -->
				<div class="rating-inline category-rating">
					<h3> Category </h3> 
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
				</div>

				<!-- category 3 -->
				<div class="rating-inline category-rating">
					<h3> Category </h3>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span> 
				</div>
			</div>

			<!-- reply section -->
			<div class="rating-section">
				<textarea class="rating-text" name="reply" cols="30" rows="10" placeholder="Write a reply..."></textarea>

				<button class="btn btn-default post">Post Rating</button>
			</div>
		</div>
		<!-- end of row -->
	</div>
</div>


<!-- start of mobile wrapper -->
<div class="wrapper mobile">
	<div>
		<div class="hero-unit">
			<!-- total job collapse group start -->
			<div class="collapse-group">
				<h4 class=" mbl-job-title"><a class="btn" data-toggle="collapse" data-target="#jobOne">JOB ONE</a></h4>
				<div class="collapse in" id="jobOne">
					<!-- description panel -->
					<div class="panel collapse-group">
						<h4 class="mbl-desc-title"><a class="btn" data-toggle="collapse" data-target="#jobDescp">Job Description</a></h4>
						<div class="collapse" id="jobDescp">
							<div class="panel-body">
								<div class="col-md-12 job-post">
									<h2 class="column-title"><i>Job Post</i></h2>
									<div class="selected-job-title">
										<h2 class="title">This is an Example Job Title</h2>

										<h4>Required Certifications</h4>
										<div class="skills-widgets-center">
											<div class="skill-qual-left">1</div>
											<div class="skill-qual-left">2</div>
											<div class="skill-qual-left">3</div>
											<div class="skill-qual-left">4</div>
										</div>
									</div>
									<div class="job-description">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
											amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
											adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
										</p>
									</div>
								</div>
							</div>
							<!-- end of panel body -->
						</div>
					</div>
					<div class="panel collapse-group">
						<h4 class="mbl-disc-title"><a class="btn" data-toggle="collapse" data-target="#jobDisc">Job Discussion</a></h4>
						<div class="collapse in" id="jobDisc">
							<div class="col-md-12 client-discussion">
								<h2 class="column-title"><i>Client Discussion</i></h2>
								<!-- converstaion section  -->
								<div class="convo-top">
									<!-- user message to the left -->
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

									<!-- member reply to the right -->
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>

									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

								</div>

								<!-- reply section -->
								<div class="reply-section">
									<textarea class="reply-text" type="text" cols="5" placeholder="Write a reply..."></textarea>
									<div class="reply-bottom">

										<label class="btn btn-default add-files">
											<input type="file" required/>
											<span><i class="fa fa-paperclip"></i> Add Files</span>
										</label>

										<button class="btn btn-default send">Send</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- discussion panel  -->
				</div>
			</div>
			<!-- total job collapse group end -->
		</div>

		<!-- job 2 -->
		<div class="hero-unit">
			<!-- total job collapse group start -->
			<div class="collapse-group">
				<h4 class=" mbl-job-title"><a class="btn" data-toggle="collapse" data-target="#jobTwo">JOB TWO</a></h4>
				<div class="collapse in" id="jobTwo">
					<!-- discussion panel -->
					<div class="panel collapse-group">
						<h4 class="mbl-disc-title"><a class="btn" data-toggle="collapse" data-target="#job2Disc">Job Discussion</a></h4>
						<!-- use collapse instead of collapse in to have open on job collapse -->
						<div class="collapse" id="job2Disc">
							<div class="col-md-12 client-discussion">
								<h2 class="column-title"><i>Client Discussion</i></h2>
								<!-- converstaion section  -->
								<div class="convo-top">
									<!-- user message to the left -->
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

									<!-- member reply to the right -->
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>

									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

								</div>

								<!-- reply section -->
								<div class="reply-section">
									<textarea class="reply-text" type="text" cols="5" placeholder="Write a reply..."></textarea>
									<div class="reply-bottom">

										<label class="btn btn-default add-files">
											<input type="file" required/>
											<span><i class="fa fa-paperclip"></i> Add Files</span>
										</label>

										<button class="btn btn-default send">Send</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- description panel -->
					<div class="panel collapse-group">
						<h4 class="mbl-desc-title"><a class="btn" data-toggle="collapse" data-target="#job2Descp">Job Description</a></h4>
						<div class="collapse" id="job2Descp">
							<div class="panel-body">
								<div class="col-md-12 job-post">
									<h2 class="column-title"><i>Job Post</i></h2>
									<div class="selected-job-title">
										<h2 class="title">This is an Example Job Title</h2>

										<h4>Required Certifications</h4>
										<div class="skills-widgets-center">
											<div class="skill-qual-left">1</div>
											<div class="skill-qual-left">2</div>
											<div class="skill-qual-left">3</div>
											<div class="skill-qual-left">4</div>
										</div>
									</div>
									<div class="job-description">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
											amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
											adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- total job collapse group end -->
		</div>
	</div>
</div>
<!-- end of mobile wrapper -->
@include('layouts.footer')
