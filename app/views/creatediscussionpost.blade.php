@include('layouts.header')

<!--START HOME JOB-BOARD CONTENT-->
<div class="wrapper">
	<div class="discussion-board-container">

		<h1 class="discussion-board-title">Discussion Board</h1>

		<div class="row">
			<!-- left column for posts -->
			<div class="col-md-8">

				<div role="tabpanel">
					<!-- Nav tabs -->
					<!-- Tab panes -->
					<div class="tab-content">
						<!-- popular posts -->
						<div role="tabpanel" class="tab-pane active" id="popular">
							<div class="post-list" style="border: 1px solid black;display: table;width:100%;">
								<div id="nd-top-instructions">
									<p>Use the form below to create a new post for the NextFab Discussion Board</p>
								</div>
								{{ Form::open(array('url' => 'discussion/post/submit')) }}
									<input type="text" name="title" placeholder="Post Title..." id="nd-title">
									<select id="nd-category" name="category">
										<option>Choose A Category</option>
										<option>Wood-Working</option>
										<option>Metal Working </option>
									</select>
									<input type="text" name="tags" placeholder="Enter tags for post seperated by commas" id="nd-tags">
									<textarea name="post" placeholder="Post Content..." id="nd-message"></textarea>
									<input type="submit" id="nd-submit">
								{{ Form::close() }}
							</div>
						</div>
					</div>
					<!-- end of post container -->
				</div>
				<!-- end of column -->
			</div>

			<!-- right column for online users-->
			<div class="contact col-md-4">
<!-- 				<div id="create-new-post-wrap">
					{{HTML::link('creatediscussionpost', 'Create New Post') }}
				</div> -->
<!-- 				<h2>
                Need help on the floor now? Contact Next Fab Support now for the fastest response.
              </h2>
				<button class="btn contact-support">Contact Support</button> -->

				<h3>Members Online</h3>
				<ul class="online-members">
					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>

					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>
					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>

					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>
					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>

					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>


				</ul>
				<!-- end of column -->
			</div>
		</div>
	</div>
</div>
@include('layouts.footer')