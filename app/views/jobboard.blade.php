@include('layouts.header')
<!--START HOME JOB-BOARD CONTENT-->
<div class="wrapper">
	<div class="job-board-container">
		<h1 class="job-board-title">Job Board</h1>
		<div class="row">
			<div class="col-md-4">
				<div class="container skills-dropdown-container">
					<form class="navbar-form" role="search">
						<input type="search" class="form-control skills-search-form">
						<button class="btn btn-default search" type="submit"><i class="glyphicon glyphicon-search"></i>
						</button>
						<i class="fa fa-bars skills-bars"></i>
					</form>

					<ul class="skills-dropdown">
						<h3 class="refineBy">Refine By Skill</h3>
						<div class="skills-check-list">
							<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill One</span>
							<br>
							<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Two</span> 
							<br>
							<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill Three</span>
							<br>
							<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Four</span> 
							<br>
						</div>
					</ul>
				</div>

				<form class="skills-form" action="" method="get">

					<input id="search-job" type="text" placeholder="Search..." />


					<label for="Skills">Refine By Skill</label>
					<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill One</span>
					<br>
					<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Two</span> 
					<br>
					<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill Three</span>
					<br>
					<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Four</span> 
					<br>

					<label class="other-options" for="Options">Other Options</label>
					<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill One</span>
					<br>
					<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Two</span>
					<br>
				</form>
			</div>
			<div class="col-md-8">
				<div class="listing">
					<h3 class="listing-title featured"> Title <i class="fa fa-star"></i></h3>
					<div class="listing-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex saepe sunt dignissimos quia, iusto, pariatur alias, voluptatibus incidunt molestias tempore quae nobis consequuntur commodi distinctio, inventore excepturi doloribus facere optio.Lorem
							ipsum dolor sit amet, consectetur adipisicing elit. Adipisci unde dignissimos rerum dolor, impedit omnis facilis qui sint cumque natus laborum a iusto, velit rem eveniet. Quisquam, quam, ducimus! Vitae!</p>

						<div class="skills-widgets-center">
							<div class="skill-qual">1</div>
							<div class="skill-qual">2</div>
							<div class="skill-qual">3</div>
							<div class="skill-qual">4</div>
						</div>

						<div class="listing-buttons">
							<a href="../individual-job/index.html">
								{{HTML::link ('individualjob', 'Learn More', array('class' => 'learn-more')) }}
<!-- 								<button class="learn-more">Learn More</button> -->
							</a>
							<a href="#">
								<button class="bid-now" data-toggle="modal" data-target="#bidModal">Bid Now</button>
							</a>
						</div>
					</div>
				</div>				
				<div class="listing">
					<h3 class="listing-title"> Title</h3>
					<div class="listing-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex saepe sunt dignissimos quia, iusto, pariatur alias, voluptatibus incidunt molestias tempore quae nobis consequuntur commodi distinctio, inventore excepturi doloribus facere optio.Lorem
							ipsum dolor sit amet, consectetur adipisicing elit. Adipisci unde dignissimos rerum dolor, impedit omnis facilis qui sint cumque natus laborum a iusto, velit rem eveniet. Quisquam, quam, ducimus! Vitae!</p>

						<div class="skills-widgets-center">
							<div class="skill-qual">1</div>
							<div class="skill-qual">2</div>
							<div class="skill-qual">3</div>
							<div class="skill-qual">4</div>
						</div>

						<div class="listing-buttons">
							<a href="../individual-job/index.html">
								{{HTML::link ('individualjob', 'Learn More', array('class' => 'learn-more')) }}
<!-- 								<button class="learn-more">Learn More</button> -->
							</a>
							<a href="#">
								<button class="bid-now" data-toggle="modal" data-target="#bidModal">Bid Now</button>
							</a>
						</div>
					</div>
				</div>

				<div class="listing">
					<h3 class="listing-title featured"> Title <i class="fa fa-star"></i></h3>
					<div class="listing-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex saepe sunt dignissimos quia, iusto, pariatur alias, voluptatibus incidunt molestias tempore quae nobis consequuntur commodi distinctio, inventore excepturi doloribus facere optio.Lorem
							ipsum dolor sit amet, consectetur adipisicing elit. Adipisci unde dignissimos rerum dolor, impedit omnis facilis qui sint cumque natus laborum a iusto, velit rem eveniet. Quisquam, quam, ducimus! Vitae!</p>

						<div class="skills-widgets-center">
							<div class="skill-qual">1</div>
							<div class="skill-qual">2</div>
						</div>

						<div class="listing-buttons">
							<a href="../individual-job/index.html">
								{{HTML::link ('individualjob', 'Learn More', array('class' => 'learn-more')) }}
<!-- 								<button class="learn-more">Learn More</button> -->
							</a>
							<a href="#">
								<button class="bid-now" data-toggle="modal" data-target="#bidModal">Bid Now</button>
							</a>
						</div>
					</div>
				<!-- bid modal -->
          <div class="modal fade" id="bidModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h2 class="modal-title" id="myModalLabel">Finalize your bid</h2>
                </div>

                <div class="modal-body">
                  <h3>
                    Thank you "UserName" for your interest in this job.
                    Fill out the optional fields below, or press "Submit" to finalize your bid
                  </h3>
                
                <!-- form -->
                <form class="form-horizontal">
                  <fieldset>

                  <!-- Resume Button --> 
                  <div class="control-group">
                    <label class="control-label" for="upload-resume">Upload Your Resume: &nbsp;</label>
                    <div class="controls">
                      <span class="btn btn-default btn-file">
                          Browse... <input type="file">
                      </span>
                    </div>
                  </div>	
									<!-- COMMENT SECTION -->
									<textarea name="comment-text" cols="30" rows="5" style="text-align: center; margin: 0 auto;display: block;"></textarea>

                  <!-- Pay Button Drop Down -->
                  <div class="control-group">
                    <label class="control-label" for="buttondropdown">Total Project Bid</label>
                    <div class="controls">
                      <div class="input-append">
                        <input id="buttondropdown" name="buttondropdown" class="input-small" placeholder="$" type="number" min="0.01" step="0.01" max="2500" value="$">
                        <div class="btn-group">
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Select P -->
                  <div class="control-group">
                    <label class="control-label" for="preferredpay">Preferred Payment Method</label>
                    <div class="controls">
                      <select id="preferredpay" name="preferredpay" class="input-small">
                        <option>Cash</option>
                        <option>Debit</option>
                        <option>Credit</option>
                        <option>Check</option>
                      </select>
                    </div>
                  </div>

                  <!-- Multiple Checkboxes -->
                  <div class="control-group">
                    <label class="control-label" for=""></label>
                    <div class="controls">
                      <label class="checkbox" for="-0">
                        <input class="checkBoxCheck" type="checkbox" name="" id="-0" value="I agree to the Terms and Services">
                        I agree to the <a href="#">Terms and Services</a>
                      </label>
                    </div>
                  </div>

                  </fieldset>
                  </form>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default bid" data-dismiss="modal">Cancel</button>
                  <button type="button" id="submit-bid" class="btn btn-primary bid">Submit Bid</button>
                </div>
              </div>
            </div>
          </div>	
<!-- 	modal end -->
					
					
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.footer')