@include('layouts.header')
    <!--START USER-PROFILE CONTENT-->
    <div class="wrapper">
      <div class="member-prof-container">
        
        <h1 class="member-prof-title">{{ $userData['username'] }}</h1>
        
        <div class="row">
         
         <!-- left column for user-info photo and skill qualif -->
          <div class="col-md-3 col-left">               
            <!-- user-profile picture -->
            <img src="{{ $userData['picture'] }}" alt="" class="user-prof">

						<a href="../chatpage/index.html"><button class="btn btn-default message-user"> Message </button></a>

            <!-- users skills -->
            <div class="skill-widgets-left">
              <div class="skill-qual-left"></div>
              <div class="skill-qual-left"></div>
              <div class="skill-qual-left"></div>
            </div>
            <!-- user overall rating -->
            <div class="rating user-ovr-rating">
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9734; </span> 
            </div> 
          </div>
        <!-- end of column -->
        
          
          <!-- right column for about and photo section-->
          <div class="col-md-9" "col-right">
            <!-- about section -->
            <div class="about">
              <h2>About</h2>
              <p class="about-body">
                <span>{{ $userData['about'] }}</span>
              </p>
            </div>
            <!-- photo-slider section -->
            <div class="photo-slider" data-jcarousel="true">
              <h2>Photos</h2>
				{{ Form::file('test', $attributes = array()) }}
              <div class="jcarousel-wrapper">
                <div class="jcarousel">
                  <ul>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                      <li style="width: 200px;"><img src="../images/logo.png" alt=""></li>
                  </ul>
               </div>

                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next">&rsaquo;</a>
              </div>
            </div>
            <!-- user-reviews section -->
            <div class="reviews">
              <h2>Reviews</h2>

              <div class="review">
                <div class="review-top">

                  <div class="rating">
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9734; </span>
                    <span>&#9734; </span> 
                  </div> 
                  <h3>Total Rating</h3>
                  
                  <h3 class="comment-date">1/19/15</h3>
                  <h3 class="comment-author">John Shmoe</h3>
                
                </div>
                <!-- category ratings -->
                <div class="review-middle">
                    <!-- category 1 -->
                  <div class="rating category-rating">
                    <h3> Category </h3>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>  
                  </div>

                  <!-- category 2 -->
                  <div class="rating category-rating">
                    <h3> Category </h3> 
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                  </div>

                  <!-- category 3 -->
                  <div class="rating category-rating">
                    <h3> Category </h3>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9734; </span>
                    <span>&#9734; </span>
                    <span>&#9734; </span> 
                  </div>
                </div>
                
                <!-- review-body -->
                <div class="review-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis voluptates commodi labore, error molestias non quidem, eligendi enim illum a deleniti esse ipsam sit, doloribus repudiandae reprehenderit fugiat ab nemo!</p>
                <p>Architecto veritatis aperiam est voluptatum assumenda. Obcaecati molestiae, et officiis neque repudiandae labore soluta porro animi! Facilis eaque quasi, eligendi aliquam, facere cum esse, eos dolores tenetur deserunt vero amet.</p>
                </p>
              </div>
              </div>
              <!-- review 2 -->
              <div class="review">
                <div class="review-top">

                  <div class="rating">
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span> 
                  </div> 
                  <h3>Total Rating</h3>
                  
                  <h3 class="comment-date">1/1/15</h3>
                  <h3 class="comment-author">Joe Doe</h3>
                
                </div>
                <!-- category ratings -->
                <div class="review-middle">
                    <!-- category 1 -->
                  <div class="rating category-rating">
                    <h3> Category </h3>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>  
                  </div>

                  <!-- category 2 -->
                  <div class="rating category-rating">
                    <h3> Category </h3> 
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9734; </span>
                  </div>

                  <!-- category 3 -->
                  <div class="rating category-rating">
                    <h3> Category </h3>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span>
                    <span>&#9733; </span> 
                  </div>
                </div>
                
                <!-- review-body -->
                <div class="review-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis voluptates commodi labore, error molestias non quidem, eligendi enim illum a deleniti esse ipsam sit, doloribus repudiandae reprehenderit fugiat ab nemo!</p>
              </div>


            <!-- end of reviews  --> 
            </div>
          <!-- end of column -->
          </div>
				</div>
			</div>
		</div>
	</div>

@include('layouts.footer')