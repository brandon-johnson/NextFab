@include('layouts.header')
<div class="container">
	{{ Form::open(array('url' => 'account/edit/submit', 'files' => true)) }}
	<center>
		<div class="form-group">
			<img width=200 height=200 src="{{ $userData['picture'] }}" alt="{{ $userData['username'] }}'s profile picture." class="img-thumbnail">
		</div>
		<div class="form-group">
			<input type="file" id="avatar" name="avatar" value="browse" placeholder="browse" />
			<p class="help-block">To update your profile picture, please select a picture from your computer.</p>
		</div>
		<div class="form-group">	
			<input type="text" disabled class="form-control" value="{{ $userData['username'] }}" />
		</div>
		<div class="form-group">
			<textarea name="about" id="about" class="form-control" rows="5" style="width: 600px;">{{ $userData['about'] }}</textarea>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" />
		</div>
	</center>
	{{ Form::close() }}
</div>
@include('layouts.footer')