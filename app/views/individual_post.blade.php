@include('layouts.header')
<!--START INDIVIDUAL JOB CONTENT-->
<div class="job-title-container">
	@foreach(Discussion::where('id', '=', $id)->get() as $post)
	<div class="selected-job-title-1" style="width:88%; margin:0 auto;">
		<h2 class="title">{{ $post->title }}</h2>

		<h4>Tags</h4>
		<div class="skills-widgets-left-1">
			<div class="skill-qual-left">1</div>
			<div class="skill-qual-left">2</div>
		</div>
	</div>
	@endforeach
</div>
<div class="container">
	@foreach(Discussion::where('id', '=', $id)->get() as $post)
	<div class="job-description-ind">
		<p>
			{{ $post->post }}
		</p>
		<!-- Modal Opener -->
		<button id="opener" style="float:right; margin-top: 10px;" type="button" class="btn btn-primary btn-lg place-bid" data-toggle="modal" data-target="#bidModal">
			Reply
		</button>

	</div>

	<!-- Modal -->
	<div class="modal fade" id="bidModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title" id="myModalLabel">Finalize your bid</h2>
				</div>

				<div class="modal-body">
					<h3>
                    Thank you "UserName" for your interest in this job.
                    Fill out the optional fields below, or press "Submit" to finalize your bid
                  </h3>

					<!-- form -->
					<form class="form-horizontal">
						<fieldset>

							<!-- Resume Button -->
							<div class="control-group">
								<label class="control-label" for="upload-resume">Upload Your Resume: &nbsp;</label>
								<div class="controls">
									<span class="btn btn-default btn-file">
                          Browse... <input type="file">
                      </span>
								</div>
							</div>

							<!-- Pay Button Drop Down -->
							<div class="control-group">
								<label class="control-label" for="buttondropdown">Button Drop Down</label>
								<div class="controls">
									<div class="input-append">
										<input id="buttondropdown" name="buttondropdown" class="input-small" placeholder="$" type="number" min="0.01" step="0.01" max="2500" value="$">
										<div class="btn-group">
											<button class="btn dropdown-toggle" data-toggle="dropdown">
												per
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="#">hr</a>
												</li>
												<li><a href="#">day</a>
												</li>
												<li><a href="#">month</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<!-- Select P -->
							<div class="control-group">
								<label class="control-label" for="preferredpay">Preferred Payment Method</label>
								<div class="controls">
									<select id="preferredpay" name="preferredpay" class="input-small">
										<option>Cash</option>
										<option>Debit</option>
										<option>Credit</option>
										<option>Check</option>
									</select>
								</div>
							</div>

							<!-- Multiple Checkboxes -->
							<div class="control-group">
								<label class="control-label" for=""></label>
								<div class="controls">
									<label class="checkbox" for="-0">
										<input type="checkbox" name="" id="-0" value="I agree to the Terms and Services">I agree to the <a href="#">Terms and Services</a>
									</label>
								</div>
							</div>

						</fieldset>
					</form>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default bid" data-dismiss="modal">Cancel</button>
					<button type="button" id="submit-bid" class="btn btn-primary bid">Submit Bid</button>
				</div>
			</div>
		</div>
	</div>
	@endforeach
</div>
@include('layouts.footer')
