@include('layouts.header')
<!--START CLIENT PORTAL CONTENT-->
<div class="wrapper desktop">
	<h2 class="member-dash-title">Job Portal</h2>
	<!-- toggle between current and previous jobs -->
	<div class="btn-group btn-toggle">
		<button id="current-jobs" class="btn btn-sm btn-primary">Current Jobs</button>
		<button id="previous-jobs" class="btn btn-sm btn-default">Previous Jobs</button>
	</div>

	<div class="row client-dash">
		<!-- jobs list : active job: grey, attn job header read with exclamation mark -->
		<div class="col-md-3 jobs-list">
			<!-- individual job -->
			<li class="job">
				<h2 class="job-title">Job Title</h2>
				<a href="#" class="client-email">johndoe@fake.com</a>
				<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
				<button class="btn btn-default view-job">View</button>
			</li>

			<li class="job active-job">
				<!-- notifications -->
				<button class="job-notifications">1</button>
				<!-- mark job as complete or on hold -->

				<ul class="notification-controls">
					<li class="mark-complete"><a href="#">Mark Complete</a>
					</li>
					<li class="hold-job"><a href="#">Hold Job</a>
					</li>
				</ul>


				<h2 class="job-title">Other Job Title</h2>
				<a href="#" class="client-email">johndoe@fake.com</a>
				<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
				<button class="btn btn-default view-job">View</button>
			</li>

			<li class="job">
				<!-- notifications -->
				<button class="job-notifications">1</button>
				<!-- mark job as complete or on hold -->

				<ul class="notification-controls">
					<li class="mark-complete"><a href="#">Mark Complete</a>
					</li>
					<li class="hold-job"><a href="#">Hold Job</a>
					</li>
				</ul>
				<h2 class="job-title job-title-attn">Other Job Title</h2>
				<a href="#" class="client-email">johndoe@fake.com</a>
				<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
				<button class="btn btn-default view-job">View</button>
			</li>
		</div>

		<!-- job post -->
		<div class="col-md-3 job-post" style="height:100%;">
			<h2 class="column-title"><i>Job Post</i></h2>

			<div class="job-post-container">
				<div class="selected-job-title">
					<h2 class="title">This is an Example Job Title</h2>

					<h4>Required Certifications</h4>
					<div class="skills-widgets-center">
						<div class="skill-qual-left">1</div>
						<div class="skill-qual-left">2</div>
						<div class="skill-qual-left">3</div>
						<div class="skill-qual-left">4</div>
					</div>
				</div>
				<div class="job-description">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
						elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
						amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
					</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
						adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
					</p>
				</div>
			</div>

		</div>
		<!-- client job-post -->
		<div class="col-md-6 create-job-post">
			<h2 class="column-title"><i>Required Info</i></h2>
			<!-- multistep form -->
			<form id="msform">
				<!-- progressbar -->
				<ul id="progressbar">
					<li class="active"></li>
					<li></li>
					<li></li>
				</ul>
				<!-- fieldsets -->
				<!-- STEP 1 FEATURE YOUR JOB -->
				<fieldset>
					<h2 class="fs-title">Get the fastest and best results by featuring your job in our Marketplace</h2>
					<h3 class="fs-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos tempore exercitationem aliquam necessitatibus, sint rerum voluptate debitis culpa iure ipsum optio voluptates vitae doloribus dolor ipsam atque nihil quidem quaerat.</h3>
					<h5><STRONG>Would you like to this Featured Job for $49.99</STRONG></h5>
					<!-- yes or no -->
					<div class="yesOrNo">
						<div class="radio-inline">
							<label>
								<input type="radio" name="optionsRadios" id="optionsRadios1" class="required" value="option1" required checked>Yes
							</label>
						</div>
						<div class="radio-inline">
							<label>
								<input type="radio" name="optionsRadios" id="optionsRadios2" class="required" value="option2" required>No
							</label>
						</div>
					</div>

					<input type="button" name="next" class="next bidDur action-button" value="Next" />

				</fieldset>
				<!-- how many bids would you like to recieve -->
				<fieldset>
					<h2 class="fs-title">How many bids would you like to recieve on this project?</h2>
					<!-- dropdown select -->
					<select id="numOfBids" class="form-control" required>
						<option>1-5 bids</option>
						<option>5-10 bids</option>
						<option>10-15 bids</option>
						<option>15-30 bids</option>
						<option>+ 30 bids</option>
					</select>
					<input type="button" name="next" class="next bidNum action-button" value="Next" />
					<input type="button" name="previous" class="previous action-button" value="Previous" />
				</fieldset>
				<!-- how long would you like to recieve the bids -->
				<fieldset>
					<h2 class="fs-title">How long would you like this project to accept bids? </h2>
					<select required id="durationOfBids" class="form-control">
						<option>1 week</option>
						<option>2 weeks</option>
						<option>1 month</option>
						<option>1-3 months</option>
						<option>3-6 months</option>
					</select>

					<input type="submit" name="submit" class="submit action-button" value="Submit" />
					<input type="button" name="previous" class="previous action-button" value="Previous" />
				</fieldset>
			</form>
		</div>




		<!-- client / member discussion -->

		<div class="col-md-6 client-discussion">
			<h2 class="column-title"><i>Client Discussion</i></h2> 
			<!-- converstaion section  -->
			<div class="convo-top">
				<!-- user message to the left -->
				<div class="user-message">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>
					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
				</div>

				<!-- member reply to the right -->
				<div class="member-reply">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>

					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
				</div>

				<div class="member-reply">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>

					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
				</div>
				<div class="user-message">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>
					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
				</div>
				<div class="member-reply">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>

					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
				</div>
				<div class="user-message">
					<div class="author">
						<h5>John Doe</h5>
						<h5>12:12pm </h5>
					</div>
					<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
				</div>

			</div>

			<!-- reply section -->
			<div class="reply-section">
				<input class="reply-text" type="text" placeholder="Write a reply...">
				<div class="reply-bottom">

					<label class="btn btn-default add-files">
						<input type="file" required/>
						<span><i class="fa fa-paperclip"></i> Add Files</span>
					</label>

					<button class="btn btn-default send">Send</button>
				</div>
			</div>
		</div>
		<!--       end of client discussion
          start of client rating -->
		<div class="col-md-6 client-rating" >
			<h2 class="column-title"><i>Client Rating</i></h2>
			<!-- converstaion section  -->
			<div class="review-middle">
				<!-- category 1 -->
				<div class="rating category-rating">
					<h3> Category </h3>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span> 
				</div>

				<!-- category 2 -->
				<div class="rating category-rating">
					<h3> Category </h3> 
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
				</div>

				<!-- category 3 -->
				<div class="rating category-rating">
					<h3> Category </h3>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span>
					<span>&#9734; </span> 
				</div>
			</div>

			<!-- reply section -->
			<div class="rating-section">
				<textarea class="rating-text" type="text" align="left" placeholder="Write a reply..."></textarea>
				<button class="btn btn-default post">Post Rating</button>
			</div>
		</div>
		<!-- end of row -->
	</div>
</div>


<!-- start of mobile wrapper -->
<div class="wrapper mobile">
	<div class="container">
		<div class="hero-unit">
			<!-- total job collapse group start -->
			<div class="collapse-group">
				<h4 class=" mbl-job-title"><a class="btn" data-toggle="collapse" data-target="#jobOne">JOB ONE</a></h4>
				<div class="collapse in" id="jobOne">
					<!-- discussion panel -->
					<div class="panel collapse-group">
						<h4 class="mbl-disc-title"><a class="btn" data-toggle="collapse" data-target="#jobDisc">Job Discussion</a></h4>
						<div class="collapse" id="jobDisc">
							<div class="col-md-12 client-discussion">
								<h2 class="column-title"><i>Client Discussion</i></h2>
								<!-- converstaion section  -->
								<div class="convo-top">
									<!-- user message to the left -->
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

									<!-- member reply to the right -->
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>

									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

								</div>

								<!-- reply section -->
								<div class="reply-section">
									<input class="reply-text" type="text" placeholder="Write a reply...">
									<div class="reply-bottom">

										<label class="btn btn-default add-files">
											<input type="file" required/>
											<span><i class="fa fa-paperclip"></i> Add Files</span>
										</label>

										<button class="btn btn-default send">Send</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- description panel  -->
					<div class="panel collapse-group">
						<h4 class="mbl-desc-title"><a class="btn" data-toggle="collapse" data-target="#jobDescp">Job Description</a></h4>
						<div class="collapse in" id="jobDescp">
							<div class="panel-body">
								<div class="col-md-12 job-post">
									<h2 class="column-title"><i>Job Post</i></h2>

									<div class="selected-job-title">
										<h2 class="title">This is an Example Job Title</h2>

										<h4>Required Certifications</h4>
										<div class="skills-widgets-center">
											<div class="skill-qual-left">1</div>
											<div class="skill-qual-left">2</div>
											<div class="skill-qual-left">3</div>
											<div class="skill-qual-left">4</div>
										</div>
									</div>
									<div class="job-description">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
											amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
											adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
										</p>
									</div>



								</div>
							</div>
							<!-- end of panel body -->
						</div>
					</div>
				</div>
			</div>
			<!-- total job collapse group end -->
		</div>

		<!-- job 2 -->
		<div class="hero-unit">
			<!-- total job collapse group start -->
			<div class="collapse-group">
				<h4 class=" mbl-job-title"><a class="btn" data-toggle="collapse" data-target="#jobTwo">JOB TWO</a></h4>
				<div class="collapse in" id="jobTwo">
					<!-- discussion panel -->
					<div class="panel collapse-group">
						<h4 class="mbl-disc-title"><a class="btn" data-toggle="collapse" data-target="#job2Disc">Job Discussion</a></h4>
						<!-- use collapse instead of collapse in to have open on job collapse -->
						<div class="collapse" id="job2Disc">
							<div class="col-md-12 client-discussion">
								<h2 class="column-title"><i>Client Discussion</i></h2>
								<!-- converstaion section  -->
								<div class="convo-top">
									<!-- user message to the left -->
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

									<!-- member reply to the right -->
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>

									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>
									<div class="member-reply">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>

										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia beatae laudantium, quisquam eos dolorum. Aut eligendi pariatur voluptas, iure distinctio voluptatum corporis. Obcaecati sint, repudiandae, tenetur nihil quidem nam soluta.</p>
									</div>
									<div class="user-message">
										<div class="author">
											<h5>John Doe</h5>
											<h5>12:12pm </h5>
										</div>
										<p class="message-mj">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eum illo animi quo voluptates nostrum mollitia perferendis, asperiores dicta ipsum reiciendis alias cumque vero. Ad alias repudiandae, explicabo est suscipit.</p>
									</div>

								</div>

								<!-- reply section -->
								<div class="reply-section">
									<input class="reply-text" type="text" placeholder="Write a reply...">
									<div class="reply-bottom">

										<label class="btn btn-default add-files">
											<input type="file" required/>
											<span><i class="fa fa-paperclip"></i> Add Files</span>
										</label>

										<button class="btn btn-default send">Send</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- description panel -->
					<div class="panel collapse-group">
						<h4 class="mbl-desc-title"><a class="btn" data-toggle="collapse" data-target="#job2Descp">Job Description</a></h4>
						<div class="collapse in" id="job2Descp">
							<div class="panel-body">
								<div class="col-md-12 job-post">
									<h2 class="column-title"><i>Job Post</i></h2>
									<div class="selected-job-title">
										<h2 class="title">This is an Example Job Title</h2>

										<h4>Required Certifications</h4>
										<div class="skills-widgets-center">
											<div class="skill-qual-left">1</div>
											<div class="skill-qual-left">2</div>
											<div class="skill-qual-left">3</div>
											<div class="skill-qual-left">4</div>
										</div>
									</div>
									<div class="job-description">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
											amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
										</p>
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
											adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- total job collapse group end -->
		</div>
	</div>
</div>
<!-- end of mobile wrapper -->

<script type="text/javascript">
	jQuery(document).ready(function(){
			$('.client-rating').hide();

	});
</script>

@include('layouts.footer')
