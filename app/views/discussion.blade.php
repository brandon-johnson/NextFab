@include('layouts.header')

<!--START HOME JOB-BOARD CONTENT-->
<div class="wrapper">
	<div class="discussion-board-container">

		<h1 class="discussion-board-title">Discussion Board</h1>

		<div class="row">
			<!-- left column for posts -->
			<div class="col-md-8">

				<div role="tabpanel">
					<!-- Nav tabs -->
					<ul id="toggle-pop-new" class="nav nav-tabs" role="tablist">
						<li role="presentation" class="show-popular active"><a href="#popular" aria-controls="home" role="tab" data-toggle="tab">Popular</a>
						</li>

						<li role="presentation" class="show-new"><a href="#new" aria-controls="profile" role="tab" data-toggle="tab">New</a>
						</li>
						<li role="presentation" class="show-new"><a href="#new" aria-controls="profile" role="tab" data-toggle="tab">New</a>
						</li>

					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<!-- popular posts -->
						<div role="tabpanel" class="tab-pane active" id="popular">

							<div class="post-list">
							@foreach(Discussion::orderBy('created_at', 'DSC')->get() as $post)
								<div class="post discussion-post">
									<!-- post up and down vote -->
									<div class="post-vote post-module">
										<div class="increment up-vote">
											<a href="{{ URL::to('/posts/' . $post->id . '/dislike') }}" class="fa fa-chevron-up"></a>
										</div>
										<div class="up-count"></div>

										<div class="down-count"></div>
										<div class="decrement down-vote">
											<a href="{{ URL::to('/posts/' . $post->id . '/dislike') }}" class="fa fa-chevron-down"></a>
										</div>
									</div>

									<!-- post user image -->
									<div class="post-user post-module">
										<img src="{{ $post->picture_url }}" alt="">
									</div>

									<!-- post question -->
									<div class="post-question post-module">
										{{ $post->post }}
									</div>
									<a href="{{ URL::to('posts/' . $post->id) }}">
										<div class="btn btn-default view-post">View Post</div>
									</a>

								</div>
								<!-- end of post -->
								@endforeach
								<!-- pagination -->
								<nav>
									<ul class="pagination">
										<li class="disabled">
											<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
											</a>
										</li>
										<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a>
										</li>
										<li><a href="#">2<span class="sr-only">(current)</span></a>
										</li>
										<li><a href="#">3 <span class="sr-only">(current)</span></a>
										</li>
										<li>
											<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
											</a>
										</li>
									</ul>
								</nav>
								<!-- end of post -->
							</div>
						</div>

						<!---------- new posts -->
						<div role="tabpanel" class="tab-pane" id="new">
							<div class="post-list">
								<div class="post discussion-post">
									<!-- post up and down vote -->
									<div class="post-vote post-module">
										<div class="increment up-vote">
											<i class="fa fa-chevron-up"></i>
										</div>
										<div class="up-count">12</div>

										<div class="down-count">-2</div>
										<div class="decrement down-vote">
											<i class="fa fa-chevron-down"></i>
										</div>
									</div>

									<!-- post user image -->
									<div class="post-user post-module">
										<img src="images/logo.png" alt="">
									</div>

									<!-- post question -->
									<div class="post-question post-module">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis nisi magnam, maxime sequi deserunt deleniti commodi aut rem fugiat omnis. Totam atque, dicta rem facilis fugit nam deleniti, velit eligendi.
									</div>
									<a href="../individual-job/index.html">
										<div class="btn btn-default view-post">View Post</div>
									</a>

								</div>
								<!-- end of post -->
								
								<div class="post discussion-post">
									<!-- post up and down vote -->
									<div class="post-vote post-module">
										<div class="increment up-vote">
											<i class="fa fa-chevron-up"></i>
										</div>
										<div class="up-count">13</div>

										<div class="down-count">-2</div>
										<div class="decrement down-vote">
											<i class="fa fa-chevron-down"></i>
										</div>
									</div>

									<!-- post user image -->
									<div class="post-user post-module">
										<img src="images/logo.pngs" alt="">
									</div>

									<!-- post question -->
									<div class="post-question post-module">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis nisi magnam, maxime sequi deserunt deleniti commodi aut rem fugiat omnis. Totam atque, dicta rem facilis fugit nam deleniti, velit eligendi.
									</div>
									<a href="../individual-job/index.html">
										<div class="btn btn-default view-post">View Post</div>
									</a>

								</div>
								<!-- end of post -->

								<div class="post discussion-post">
									<!-- post up and down vote -->
									<div class="post-vote post-module">
										<div class="increment up-vote">
											<i class="fa fa-chevron-up"></i>
										</div>
										<div class="up-count">12</div>

										<div class="down-count">-2</div>
										<div class="decrement down-vote">
											<i class="fa fa-chevron-down"></i>
										</div>
									</div>

									<!-- post user image -->
									<div class="post-user post-module">
										<img src="images/logo.png" alt="">
									</div>

									<!-- post question -->
									<div class="post-question post-module">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis nisi magnam, maxime sequi deserunt deleniti commodi aut rem fugiat omnis. Totam atque, dicta rem facilis fugit nam deleniti, velit eligendi.
									</div>

									<a href="../individual-job/index.html">
										<div class="btn btn-default view-post">View Post</div>
									</a>

								</div>
								<!-- end of post -->
								<!-- pagination -->
								<nav>
									<ul class="pagination">
										<li class="disabled">
											<a href="#" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
											</a>
										</li>
										<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a>
										</li>
										<li><a href="#">2<span class="sr-only">(current)</span></a>
										</li>
										<li><a href="#">3 <span class="sr-only">(current)</span></a>
										</li>
										<li>
											<a href="#" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
											</a>
										</li>
									</ul>
								</nav>

							</div>
							<!-- end of tab content -->
						</div>
						<!-- end of tab panel -->
					</div>
					<!-- end of post container -->
				</div>
				<!-- end of column -->
			</div>

			<!-- right column for online users-->
			<div class="contact col-md-4">
				<div id="create-new-post-wrap">
					{{HTML::link('discussion/post', 'Create New Post') }}
				</div>
<!-- 				<h2>
                Need help on the floor now? Contact Next Fab Support now for the fastest response.
              </h2>
				<button class="btn contact-support">Contact Support</button> -->

				<h3>Members Online</h3>
				<ul class="online-members">
					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>

					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>
					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>

					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>
					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>

					<li class="user pop-over" data-toggle="popover" data-placement="top">
						<img src="images/logo.png" alt="" class="user-prof-right">
						<label class="member-name">Member Name</label>
						<div id="popover_content_wrapper" style="display: none">
							<img class="user-prof-right" src="images/logo.png" alt="">
							<h2 class="member-name"> John Doe</h2>
							<div class="skills-widgets-left">
								<div class="skill-qual-left">1</div>
								<div class="skill-qual-left">2</div>
								<div class="skill-qual-left">3</div>
								<div class="skill-qual-left">4</div>
							</div>
							<div class="mini-prof-bottom">
								<a href="../chatpage/index.html">
									<button class="btn message-user-right">Message</button>
								</a>
								<a href="../member-profile/index.html">
									<button class="btn view-profile-right">View Profile</button>
								</a>
							</div>
						</div>
					</li>


				</ul>
				<!-- end of column -->
			</div>
		</div>
	</div>
</div>
@include('layouts.footer')