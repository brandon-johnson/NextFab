@include('layouts.header')
<!--START ADMIN PORTAL CONTENT-->
<div class="wrapper desktop">
	<h2 class="admin-portal-title">Job Approval</h2>

	<!-- toggle between current and previous jobs -->
	<div class="btn-group btn-toggle">
		<button id="current-jobs" class="btn btn-sm btn-primary">Current Jobs</button>
		<button id="previous-jobs" class="btn btn-sm btn-default">Previous Jobs</button>
	</div>
	<div class="container">
		<div class="row admin-portal container">
			<!-- jobs list : active job: grey, attn job header read with exclamation mark -->
			<div class="col-md-3 jobs-list">
				<!-- individual job -->
				<li class="job">
					<h2 class="job-title job-title">Job Title</h2>
					<a href="#" class="client-email">johndoe@fake.com</a>
					<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
					<button class="btn btn-default view-job">View</button>
				</li>

				<li class="job active-job">
					<h2 class="job-title">Other Job Title</h2>
					<a href="#" class="client-email">johndoe@fake.com</a>
					<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
					<button class="btn btn-default view-job">View</button>
				</li>

				<li class="job">
					<h2 class="job-title job-title-attn">Other Job Title</h2>
					<a href="#" class="client-email">johndoe@fake.com</a>
					<span class="start-date"><strong>Start Date:</strong> 1-21-15</span>
					<button class="btn btn-default view-job">View</button>
				</li>
			</div>

			<div class="col-md-9 job-post" style="height: 100%;">
				<!-- start of job post title and description -->
				<div class="job-post-container">
					<h2 id="job-post-title" class="column-title"><i>Job Post</i>                
             <!-- Approve or deny buttons -->
             <div class="approve-delete">  
                <button id="deny-job" class="btn btn-sm" data-toggle="modal" data-target="#denialModal" data-whatever="@getbootstrap">Deny</button>
                <button id="approve-job" class="btn btn-sm btn-primary">Approve</button>
              
              <!-- denial modal -->
                <div class="modal fade" id="denialModal" tabindex="-1" role="dialog" aria-labelledby="denyLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title" id="denyLabel">Denial message</h4>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="form-group">
                            <label for="recipient-name" class="control-label">Recipient:</label>
                            <input type="text" class="form-control" placeholder="Client User Name" id="recipient-name">
                          </div>
                          <div class="form-group">
                            <label for="message-text" class="control-label">Message:</label>
                            <textarea class="form-control" id="message-text" rows="10" cols="50" placeholder="We are sorry to inform you that your job has not been approved because ..."></textarea>
                          </div>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="border-radius:0;">Close</button>
                        <button type="button" class="btn btn-primary" style="border-radius:0;">Send message</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </h2>
					<!-- JOB TITLE -->
					<div class="selected-job-title">
						<h2 class="title">This is an Example Job Title</h2>
						<h4>Required Certifications</h4>
						<div class="skills-widgets-center">
							<div class="skill-qual-left">1
								<input class="skill-check-box" type="checkbox" name="skill1" value="skill1">
							</div>
							<div class="skill-qual-left">2
								<input class="skill-check-box" type="checkbox" name="skill2" value="skill2">
							</div>
							<div class="skill-qual-left">3
								<input class="skill-check-box" type="checkbox" name="skill3" value="skill3">
							</div>
							<div class="skill-qual-left">4
								<input class="skill-check-box" type="checkbox" name="skill4" value="skill4">
							</div>
							<div class="skill-qual-left">5
								<input class="skill-check-box" type="checkbox" name="skill5" value="skill5">
							</div>
						</div>
					</div>
					<div class="job-description">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
							elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
							amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
							adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
						</p>
					</div>
				</div>
				<!-- end of job description container -->
			</div>
			<!-- end of row -->
		</div>
	</div>
</div>
@include('layouts.footer')