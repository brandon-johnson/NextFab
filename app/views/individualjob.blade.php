@include('layouts.header')
<!--START INDIVIDUAL JOB CONTENT-->
<div class="job-title-container">
	<div class="selected-job-title-1" style="width:88%; margin:0 auto;">
		<h2 class="title">This is an Example Job Title</h2>

		<h4>Required Certifications</h4>
		<div class="skills-widgets-left-1">
			<div class="skill-qual-left">1</div>
			<div class="skill-qual-left">2</div>
			<div class="skill-qual-left">3</div>
			<div class="skill-qual-left">4</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="job-description-ind">
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore soluta nisi nemo suscipit voluptate sed ducimus ullam ipsa similique consequatur, obcaecati quae doloremque commodi, officiis porro modi quas, et aliquid? Lorem ipsum dolor sit amet,
			consectetur adipisicing elit. Sapiente voluptatibus facilis voluptatem sint laboriosam sequi nesciunt deserunt, soluta necessitatibus impedit, laborum voluptates, obcaecati a distinctio porro dolorem tenetur earum. Velit!
		</p>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus nobis, iste beatae magni laborum sunt enim dolores deserunt, numquam quae minus quos officia, dignissimos adipisci harum laboriosam a eos at. Lorem ipsum dolor sit amet, consectetur adipisicing
			elit. In ab at culpa tenetur mollitia quis, a assumenda, perferendis accusamus nisi pariatur saepe molestias velit, ut veniam? Molestiae, perspiciatis eos fugiat.
		</p>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas doloribus obcaecati ad perferendis voluptatibus placeat molestias ullam ex. Molestiae enim corrupti perspiciatis amet possimus excepturi, minima aut est error expedita. Lorem ipsum dolor sit
			amet, consectetur adipisicing elit. Optio repellat possimus quaerat facilis iste. Iure dolores, esse voluptatibus alias delectus aliquam iusto officiis porro repellendus ullam ratione, itaque, tempore sapiente.
		</p>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum vero explicabo, neque delectus eligendi totam, cumque. Velit impedit debitis sapiente, blanditiis ipsa odio facilis, iure, quas ab doloremque dicta, neque. Lorem ipsum dolor sit amet, consectetur
			adipisicing elit. Debitis beatae quod earum aut, autem? Dicta dolore voluptatum error aut reprehenderit perspiciatis, praesentium, sequi dolorem iusto totam, explicabo placeat delectus. Fuga.
		</p>

		<!-- Modal Opener -->
		<button id="opener" type="button" class="btn btn-primary btn-lg place-bid" data-toggle="modal" data-target="#bidModal">
			Place Bid
		</button>

	</div>

	<!-- Modal -->
	<div class="modal fade" id="bidModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="modal-title" id="myModalLabel">Finalize your bid</h2>
				</div>

				<div class="modal-body">
					<h3>
                    Thank you "UserName" for your interest in this job.
                    Fill out the optional fields below, or press "Submit" to finalize your bid
                  </h3>

					<!-- form -->
					<form class="form-horizontal">
						<fieldset>

							<!-- Resume Button -->
							<div class="control-group">
								<label class="control-label" for="upload-resume">Upload Your Resume: &nbsp;</label>
								<div class="controls">
									<span class="btn btn-default btn-file">
                          Browse... <input type="file">
                      </span>
								</div>
							</div>

							<!-- Pay Button Drop Down -->
							<div class="control-group">
								<label class="control-label" for="buttondropdown">Button Drop Down</label>
								<div class="controls">
									<div class="input-append">
										<input id="buttondropdown" name="buttondropdown" class="input-small" placeholder="$" type="number" min="0.01" step="0.01" max="2500" value="$">
										<div class="btn-group">
											<button class="btn dropdown-toggle" data-toggle="dropdown">
												per
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="#">hr</a>
												</li>
												<li><a href="#">day</a>
												</li>
												<li><a href="#">month</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<!-- Select P -->
							<div class="control-group">
								<label class="control-label" for="preferredpay">Preferred Payment Method</label>
								<div class="controls">
									<select id="preferredpay" name="preferredpay" class="input-small">
										<option>Cash</option>
										<option>Debit</option>
										<option>Credit</option>
										<option>Check</option>
									</select>
								</div>
							</div>

							<!-- Multiple Checkboxes -->
							<div class="control-group">
								<label class="control-label" for=""></label>
								<div class="controls">
									<label class="checkbox" for="-0">
										<input type="checkbox" name="" id="-0" value="I agree to the Terms and Services">I agree to the <a href="#">Terms and Services</a>
									</label>
								</div>
							</div>

						</fieldset>
					</form>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default bid" data-dismiss="modal">Cancel</button>
					<button type="button" id="submit-bid" class="btn btn-primary bid">Submit Bid</button>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="container">
	<div class="job-discussion">
		<div class="discussion-top">
			<h1> Job Discussion </h1>
			<i class="fa fa-plus-square-o convo-dropdown"></i>
		</div>

		<div class="job-convo">
			<!-- initial post -->
			<div class="disc-body contractor-body">
				<img class="user-img" src="../images/logo.png" alt="">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sapiente neque ducimus dolores illo iste assumenda eos iusto ipsam quo sequi officiis quos, perferendis, vel. Quasi mollitia possimus, corporis quidem? Lorem ipsum dolor sit amet, consectetur
					adipisicing elit. Ex ad voluptatum illo maiores, id modi nulla magni optio molestias earum temporibus facere sint quia perferendis harum nam quidem reiciendis eveniet!</p>
				<div class="disc-bottom">
					<a href="#">Reply</a>
					<a href="#">Like (0)</a>
				</div>
			</div>
			<!-- reply post -->
			<div class="disc-body anony-body">
				<img class="user-img" src="../images/logo.png" alt="">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sapiente neque ducimus dolores illo iste assumenda eos iusto ipsam quo sequi officiis quos, perferendis, vel. Quasi mollitia possimus, corporis quidem?</p>
				<div class="disc-bottom">
					<a href="#">Reply</a>
					<a href="#">Like (0)</a>
				</div>
			</div>
			<div class="disc-body contractor-body">
				<img class="user-img" src="../images/logo.png" alt="">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sapiente neque ducimus dolores illo iste assumenda eos iusto ipsam quo sequi officiis quos, perferendis, vel. Quasi mollitia possimus, corporis quidem? Lorem ipsum dolor sit amet, consectetur
					adipisicing elit. Ex ad voluptatum illo maiores, id modi nulla magni optio molestias earum temporibus facere sint quia perferendis harum nam quidem reiciendis eveniet!</p>
				<div class="disc-bottom">
					<a href="#">Reply</a>
					<a href="#">Like (0)</a>
				</div>
			</div>
			<!-- reply post -->
			<div class="disc-body anony-body">
				<img class="user-img" src="../images/logo.png" alt="">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sapiente neque ducimus dolores illo iste assumenda eos iusto ipsam quo sequi officiis quos, perferendis, vel. Quasi mollitia possimus, corporis quidem? Lorem ipsum dolor sit amet, consectetur
					adipisicing elit. Ut quo voluptatem, atque autem. Molestiae reprehenderit facere, eos commodi, voluptatibus dolor quae. Eaque rerum a dolorum soluta obcaecati molestiae beatae iste!</p>
				<div class="disc-bottom">
					<a href="#">Reply</a>
					<a href="#">Like (0)</a>
				</div>
			</div>
			<!-- end of convo list -->
			<div class="add-convo">
				<textarea name="addToConvo" cols="30" rows="5" style="text-align:left"></textarea>
				<button class="btn btn-default">Comment</button>
			</div>
		</div>
	</div>
</div>
@include('layouts.footer')
