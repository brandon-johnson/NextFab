@include('layouts.header')
<!--START HOME PAGE CONTENT-->
    <div class="wrapper">
      <div>
          <!-- CTA's -->
          <div class="row top">
		  @if(Auth::check())
              <div class="col-md-4">
                <div id="home-jb-cta-block">
                  <div class="cta-block-inner">
                    <h1>
                      Job Board
                    </h1>
                    <a href="{{ URL::to('jobboard') }}">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div id="home-md-cta-block">
                  <div class="cta-block-inner">
                    <h1>
                      Member Directory
                    </h1>
                    <a href="{{ URL::to('directory') }}">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div id="home-db-cta-block">
                  <div class="cta-block-inner">
                    <h1>
                      Discussion Board
                    </h1>
                    <a href="{{ URL::to('discussion') }}">Learn More</a>
                  </div>
                </div>
               </div>
			  @else
              <div class="col-md-4">
                <div id="home-jb-cta-block">
                  <div class="cta-block-inner">
                    <h1>
                      Job Board
                    </h1>
                    <a href="#NotLoggedIn">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div id="home-md-cta-block">
                  <div class="cta-block-inner">
                    <h1>
                      Member Directory
                    </h1>
                    <a href="#NotLoggedIn">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div id="home-db-cta-block">
                  <div class="cta-block-inner">
                    <h1>
                      Discussion Board
                    </h1>
                    <a href="#NotLoggedIn">Learn More</a>
                  </div>
                </div>
              </div>			  
			@endif
          </div>
          <!-- EVENTS AND NEWS -->
          <div class="row bottom">
              <div class="col-md-6">
                <div id="home-events">
                  <h1 id="main-events-title">
                    Events
                  </h1>
                  <div class="idv-home-event-wrap">
                    <img src="images/event-1.jpg" class="event-pic">
                    <div class="event-right-stuff">
                      <h1 class="idv-event-title">
                        Event Number One
                      </h1>
                      <p class="event-desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at dui fringilla.
                      </p>
                      <a href="#" class="event-cta">Learn More</a>
                    </div>
                  </div>
                  <div class="idv-home-event-wrap">
                    <img src="images/event-2.jpg" class="event-pic">
                    <div class="event-right-stuff">
                      <h1 class="idv-event-title">
                        Event Number One
                      </h1>
                      <p class="event-desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at dui fringilla.
                      </p>
                      <a href="#" class="event-cta">Learn More</a>
                    </div>
                  </div>
                  <div class="idv-home-event-wrap">
                    <img src="images/event-3.jpg" class="event-pic">
                    <div class="event-right-stuff">
                      <h1 class="idv-event-title">
                        Event Number One
                      </h1>
                      <p class="event-desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at dui fringilla.
                      </p>
                      <a href="#" class="event-cta">Learn More</a>
                    </div>
                  </div>
              </div>
            </div>
            <!-- NEWS COLUMN -->
            <div class="col-md-6 news-column">
              <div id="news-list">
                <h1 id="news-title">News</h1>
                <div class="row">
                  <div class="col-md-6 first-job">
                    <img src="images/event-1.jpg" alt="" width="100%;">
                    <li>
                    <a href="#">Next Fab News Story #1</a>
                      <p class="featured-news-story">
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium iure repellendus perspiciatis explicabo aliquid illo aperiam inventore repudiandae, praesentium dolore voluptatum, quo quasi itaque nisi at. Dolor amet suscipit aut...<a href="#"> Read More Here...</a></span>
                      </p>
                    </li>
                      
                  </div>
                
                  <div class="col-md-6 other-jobs">
                    <ul>
                      <li>
                        <a href="#">Next Fab News Story #2</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </li>
                      <li>
                        <a href="#">Next Fab News Story #3</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </li>
                      <li>
                        <a href="#">Next Fab News Story #4</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </li
                      >
                      <li>
                        <a href="#">Next Fab News Story #5</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </li>
                      <li>
                        <a href="#">Next Fab News Story #6</a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </li>
                    </ul>
                  </div>
              </div>
            </div>
          </div>
      </div>
    </div>        
</div>
@include('layouts.footer')