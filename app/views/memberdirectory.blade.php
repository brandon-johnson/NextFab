@include('layouts.header')

<!--START MEMBER-DIRECTORY CONTENT-->
<div class="wrapper">
	<div class="member-board-container">
		<h1 class="member-board-title">Member Directory</h1>
		<div class="row">
			<div class="col-md-4">
				<div class="container skills-dropdown-container">
					<form class="navbar-form" role="search">
						<input type="search" class="form-control skills-search-form">
						<button class="btn btn-default search" type="submit"><i class="glyphicon glyphicon-search"></i>
						</button>
						<i class="fa fa-bars skills-bars"></i>
					</form>

					<ul class="skills-dropdown">
						<h3 class="refineBy">Refine By Skill</h3>
						<div class="skills-check-list">
							<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill One</span>
							<br>
							<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Two</span> 
							<br>
							<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill Three</span>
							<br>
							<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Four</span> 
							<br>
						</div>
					</ul>
				</div>

				<!-- skills-form section -->
				<form class="skills-form" action="" method="get">

					<input id="search-job" type="text" placeholder="Search..." />

					

					<label for="Skills">Refine By Skill</label>

					<div class="skills-check-list">
						<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill One</span>
						<br>
						<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Two</span> 
						<br>
						<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill Three</span>
						<br>
						<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Four</span> 
						<br>
					</div>

					<label class="other-options" for="Options">Other Options</label>

					<div class="skills-check-list">
						<input type="checkbox" name="skills" value="skill-one"><span class="skill-checks">Skill One</span>
						<br>
						<input type="checkbox" name="skills" value="skill-two" checked="checked"><span class="skill-checks">Skill Two</span>
						<br>
					</div>
				</form>
			</div>
			
			<div class="col-md-8">
				@foreach(User::orderBy('created_at', 'DSC')->get() as $user)
				<div class="user">
					<div class="user-title">
						<div class="prof-pic">
							<img src="{{ $user->picture_url }}" alt="profile picture">
						</div>
						<h2>{{ $user->username }}</h2>
					</div>
					<div class="user-body">
						<div class="skills-widgets">
							<div class="skill-qual skill-1">1</div>
							<div class="skill-qual skill-2">2</div>
							<div class="skill-qual skill-3">3</div>
							<div class="skill-qual skill-4">4</div>
						</div>
						<p>{{ $user->about }}</p>
						<div class="listing-buttons">
							<a href="{{ URL::to('profile/' . $user->username)}}">
								<button class="button view-profile">View Profile</button>
							</a>
							<a href="../chatpage/index.html">
								<button class="button message">Message</button>
							</a>
						</div>
					</div>
				</div>
				@endforeach
				<!--           end of user -->
			</div>
		</div>
	</div>
</div>

@include('layouts.footer')