<!DOCTYPE html>
<html>

<head>
	<title>Next Fab | Connect</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/logo.png" type="image/icon">

	<!--CSS LINKS-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.min.css">
	<link href='http://fonts.googleapis.com/css?family=Exo+2:400,100,300,600,700,400italic,800|Vollkorn' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">{{ HTML::style('css/style.css'); }}





	<!--JS LINKS-->
	{{ HTML::script('http://code.jquery.com/jquery-1.11.3.min.js'); }} {{ HTML::script('js/carousel.js'); }} {{ HTML::script('js/carousel-responsive.js'); }} {{ HTML::script('js/site.js'); }} {{ HTML::script('js/dropdown-hover.min.js'); }} {{ HTML::script('js/discussion-board.js');
	}} {{ HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'); }} {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js'); }}
</head>

<body>
	<!--NAVIGATION-->
	<div class="navbar navbar-inverse navbar-static-top" role="navigation">
		<div id="top-nav">
			<div class="wrapper">
				{{ HTML::image('images/logo.png', 'Next Fab Logo', array('alt' => 'Next Fab Logo', 'id' => 'nav-logo')) }}
				<div id="nav-top-right">
					<ul class="nav navbar-nav" id="top-right-widgets">
						<!--
					0: regular
					1: admin
					2: job poster
					3:
				-->@if(Auth::check())
						
						<li>
							<img id="blank-avatar" src="{{ Auth::user()->picture_url }}" alt="{{ Auth::user()->username }}'s Profile Picture.">
							<!-- <label>Name</label> -->
						</li>
						<a style="text-decoration: none;" href="{{ URL::to('/profile/' . Auth::user()->username) }}">
							<li class="loginSignup">{{ Auth::user()->username }}</li>
						</a>
						<!-- MAIL -->
						<li class="mail mail-pop-over" data-container="body" data-toggle="popover" data-placement="bottom"><i class="fa fa-envelope-o"></i>
						</li>
						<div id="mail_content_wrapper" style="display: none">
							<li class="message-item" id="message1">
								<img class="usr-msg-img" src="/images/avatar_blank.png" alt=""><a href="#"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. </a>
							</li>
							<li class="message-item" id="message2">
								<img class="usr-msg-img" src="/images/avatar_blank.png" alt=""> <a href="#"> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
							</li>
							<li class="message-item" id="message3">
								<img class="usr-msg-img" src="/images/avatar_blank.png" alt=""><a href="#"> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
							</li>
							<li class="view-more"><a href="chatpage/index.html">View More</a>
							</li>
						</div>
						<!-- SETTINGS -->
						<li class="settings settings-pop-over" data-container="body" data-toggle="popover" data-placement="bottom"><i class="fa fa-gear"></i> 
						</li>

						<div id="settings_content_wrapper" style="display: none">

							<li class="settings-item" id="accountSettings"><a href="{{ URL::to('account/edit') }}"> Account Settings </a>
							</li>
							<li class="settings-item" id="logOut"> <a href="{{ URL::to('logout') }}"> Log Out</a>
							</li>
							<li class="settings-item" id="helpButton"><a href="#"> Help</a>
							</li>
							<li class="settings-item" id="reportProblem"><a href="#">Report Problem</a>
							</li>

						</div>
						@else
						<li class="loginSignup" href="#signup" data-toggle="modal" data-target=".bs-modal-sm">Sign In</li>
						@endif
					</ul>
				</div>
			</div>
		</div>

		@if(Auth::check())
		@if(Auth::user()->is_admin == 0)
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
				<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active">{{ HTML::link('', 'Home') }}
					<!-- 			  <a href="../index.html">Home</a> -->
				</li>
				<li><a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Jobs <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>{{ HTML::link('jobboard', 'Job Board') }}
							<!-- 				  <a href="../job-board/index.html">Job Board</a> -->
						</li>
						<li>{{ HTML::link('myjobs', 'My Jobs') }}
							<!-- 				  <a href="../member-dashboard/index.html">My Jobs</a>-->
						</li>
					</ul>
				</li>
				<li>{{ HTML::link('directory', 'Member Directory') }}
					<!-- 			  <a href="../member-directory/index.html">Member Directory</a> -->
				</li>
				<li>{{ HTML::link('discussion', 'Discussion Board') }}
					<!-- 			  <a href="../discussion-board/index.html">Discussion Board</a> -->
				</li>
			</ul>

			<div class="col-sm-3 col-md-3 pull-right">
				<form class="navbar-form" role="search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
						<div class="input-group-btn">
							<button class="btn btn-default search" type="submit"><i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	@elseif(Auth::user()->is_admin == 1)
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active">{{ HTML::link('', 'Home') }}
					<!-- 			  <a href="../index.html">Home</a> -->
				</li>
				<li><a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Jobs <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>{{ HTML::link('jobboard', 'Job Board') }}
							<!-- 				  <a href="../job-board/index.html">Job Board</a> -->
						</li>
						<li>{{ HTML::link('myjobs', 'My Jobs') }}
							<!-- 				  <a href="../member-dashboard/index.html">My Jobs</a>-->
						</li>
					</ul>
				</li>
				<li>{{ HTML::link('directory', 'Member Directory') }}
					<!-- 			  <a href="../member-directory/index.html">Member Directory</a> -->
				</li>
				<li>{{ HTML::link('discussion', 'Discussion Board') }}
					<!-- 			  <a href="../discussion-board/index.html">Discussion Board</a> -->
				</li>
				<li>{{ HTML::link('jobapproval', 'Job Approval') }}
					<!-- 			  <a href="../admin-portal/index.html">Job Approval</a> -->
				</li>
			</ul>

			<div class="col-sm-3 col-md-3 pull-right">
				<form class="navbar-form" role="search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
						<div class="input-group-btn">
							<button class="btn btn-default search" type="submit"><i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	@elseif(Auth::user()->is_admin == 2)
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active">{{ HTML::link('', 'Home') }}
					<!-- 			  <a href="../index.html">Home</a> -->
				</li>
<!-- 				<li><a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">Jobs <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>{{ HTML::link('myjobs', 'My Jobs') }}
						  <a href="../member-dashboard/index.html">My Jobs</a>
						</li>
					</ul>
				</li> -->
			</ul>

			<div class="col-sm-3 col-md-3 pull-right">
				<form class="navbar-form" role="search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
						<div class="input-group-btn">
							<button class="btn btn-default search" type="submit"><i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endif()

	@else
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active">{{ HTML::link('', 'Home') }}
					<!-- 			  <a href="../index.html">Home</a> -->
				</li>
			</ul>

			<div class="col-sm-3 col-md-3 pull-right">
				<form class="navbar-form" role="search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
						<div class="input-group-btn">
							<button class="btn btn-default search" type="submit"><i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endif
	<!-- nav bottom -->



	<!-- Sign In Modal -->
	<div class="modal fade bs-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<br>
				<div class="bs-example bs-example-tabs">
					<h2 id="si-mod-title">Sign In</h2>
					
				</div>
				<div class="modal-body">
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane fade active in" id="signin">
							{{ Form::open(array('url' => 'login', 'class' => 'form-horizontal')) }}
							<fieldset>
								<!-- Sign In Form -->
								<div class="control-group">
									<label class="control-label" for="userid">User Name:</label>
									<div class="controls">
										<input required="" id="username" name="username" type="text" class="form-control" placeholder="username" class="input-medium" required="">
									</div>
								</div>

								<!-- Password input-->
								<div class="control-group">
									<label class="control-label" for="passwordinput">Password:</label>
									<div class="controls">
										<input required="" id="password" name="password" class="form-control" type="password" placeholder="********" class="input-medium">
									</div>
								</div>

								<!-- Multiple Checkboxes (inline) -->
								<div class="control-group">
									<label class="control-label" for="rememberme"></label>
									<div class="controls">
										<label class="checkbox inline" for="rememberme-0">
											<input type="checkbox" name="rememberme" id="rememberme-0" value="Remember me">Remember me
										</label>
									</div>
								</div>
								<!-- Button -->
								<div class="control-group">
									<label class="control-label" for="signin"></label>
									<div class="controls">
										<button id="signin" name="signin" class="btn btn-success">Sign In</button>
									</div>
								</div>
							</fieldset>
							{{ Form::close() }}
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<center>
						<button class="close" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</center>
				</div>
			</div>
		</div>
	</div>
	<div class="container" id="messagebox">
		@if ($message = Session::get('message')) {{ $message }} @else @endif
	</div>
