<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Home
Route::get('/', 'FunctionsController@getIndex');
//MY JOBS
Route::get('/myjobs', 'FunctionsController@getMyJobs');
//MY JOBS
Route::get('/jobboard', 'FunctionsController@getJobBoard');
//Member Directory
Route::get('/directory', 'FunctionsController@getMemberDirectory');
//Discussion Board
Route::get('/discussion', 'FunctionsController@getDiscussionBoard');
//Create New Discussion
Route::get('/discussion/post','FunctionsController@getCreateDiscussion');
Route::post('/discussion/post/submit', 'FunctionsController@postSubmitDiscussion');
//Viewing Posts
Route::get('/posts/{id}', 'FunctionsController@getViewPost');
Route::get('/posts/{id}/like', 'FunctionsController@getUpvote');
Route::get('/posts/{id}/dislike', 'FunctionsController@getDownvote');
//Job Approval
Route::get('/jobapproval', 'FunctionsController@getJobApproval');
//Individual Job
Route::get('/individualjob', 'FunctionsController@getIndividualJob');
//Client Dashboard
Route::get('/clientdashboard','FunctionsController@getClientDash');
//Registration
Route::get('/register/{AUTH_KEY}', 'FunctionsController@getRegister');
//Authentication
Route::post('/login', 'FunctionsController@postLogin');

Route::post('/register/submit', 'FunctionsController@postRegister');

Route::get('/logout', 'FunctionsController@getLogout');

Route::get('/profile/{username}', 'FunctionsController@getProfile');

Route::get('/account/edit', 'FunctionsController@getEditProfile');

Route::post('/account/edit/submit', 'FunctionsController@postEditProfile');