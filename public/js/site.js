//////////////////////////////////////////////////
//THIS IS WHAT WAS HERE BEFORE JOE ADDED THE REST OF THE JS WE NEED TO DOCUMENT WHAT THIS DOES PLEASE

$(function() {
/* 	accordion for collapse pages */
  $( "#accordion" ).accordion({
    collapsible: true
  });
});

$(document).ready(function(){
  $('.skills-bars').on('click',function(){
    $('.skills-dropdown').slideToggle();
  });
/* 	Settings pop under */
  $('.settings-pop-over').popover({
      html: true,
      content: function() {
        return $('#settings_content_wrapper').html();
      }
    });
/* 	Mail pop under */
  $('.mail-pop-over').popover({
    html: true,
    content: function() {
      return $('#mail_content_wrapper').html();
    }
  });
});

//////////////////////////////////////////////////
//INDIVIDUAL JOB

$(document).ready(function(){
    $('.convo-dropdown').on('click', function() {
      $(this).toggleClass('fa-plus-square-o fa-minus-square-o');
      $('.job-convo').slideToggle();
    });
});

//////////////////////////////////////////////////
//MEMBER DASHBOARD AKA MY JOBS

$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    
    if ($(this).find('.btn-primary').size()>0) {
        $(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').size()>0) {
        $(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').size()>0) {
        $(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').size()>0) {
        $(this).find('.btn').toggleClass('btn-info');
    }
    
    $(this).find('.btn').toggleClass('btn-default');
       
});

/* $('form').submit(function(){
    alert($(this["options"]).val());
    return false;
}); */

$(document).ready(function(){
    //$('.client-rating').hide();
    var windowSize = $(window).width();
    
    if ($('h2.job-title').is('.job-title-attn') === true){
        $('.job-title-attn').siblings('.job-notifications').hide();
        $('.job-title-attn').parent().append($('<a href="#"> <i class="fa fa-exclamation"> </i> </a>'));
        $('.client-discussion').hide();
    }

    if (windowSize < 970){
        $('.client-discussion').show();
        $('.client-rating').hide();
    } else if (windowSize > 970){
        $('.client-rating').show();
        // $('.client-discussion').hide();
    }

    $('.job-title-attn').on('click', function(){
        $('.client-rating').toggle();
        $('.client-discussion').toggle();
    });
	
		$('.view-job').on('click', function(){
        $('.create-job-post').toggle();
        $('.client-discussion').show();
				$('.client-rating').hide();
        $(this).parent().toggleClass('active-job');
        $(this).parent().siblings().removeClass('active-job');
    });
	
	   // JOB NOTIFICATION CONTROLS
    $('.job-notifications').on('click', function(){
        $(this).siblings('.notification-controls').slideToggle();
    });

});

//////////////////////////////////////////////////
//DISCUSSION BOARD

$(function(){
  $('.user')
    .hover(function(){
      $(this).children('.pointer').toggle();
      $(this).children('.user-info').toggle();
      });    
    $('.pop-over').popover({
      html: true,
      content: function() {
        return $('#popover_content_wrapper').html();
      }
    });

});

//////////////////////////////////////////////////
//ADMIN APPROVAL PAGE

// // CALLBACK FUNCTIONS

function buttonToggle(){
    $('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    
    if ($(this).find('.btn-primary').size()>0) {
        $(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').size()>0) {
        $(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').size()>0) {
        $(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').size()>0) {
        $(this).find('.btn').toggleClass('btn-info');
    }
    
    $(this).find('.btn').toggleClass('btn-default');
       });

/*     $('form').submit(function(){
            alert($(this["options"]).val());
        return false;
    }); */
}

// ON DOCUMENT READY

$(document).ready(function(){
    buttonToggle();
    $('.notification-controls').hide();
    // click the view button under jobs to show details and conversation
    $('.view-job').on('click', function(){
        $('.create-job-post').toggle();
        $('.client-discussion').toggle();
        $(this).parent().toggleClass('active-job');
        $(this).parent().siblings().removeClass('active-job');
    });

    // JOB NOTIFICATION CONTROLS
    $('.job-notifications').on('click', function(){
        $(this).siblings('.notification-controls').slideToggle();
    });

    // END OF JOB TITLE ATTENTION

});

//////////////////////////////////////////////////
//INDIVIDUAL JOB PAGE

/* $(document).ready(function(){
    $('.convo-dropdown').on('click', function() {
      $(this).toggleClass('fa-plus-square-o fa-minus-square-o');
      $('.job-convo').slideToggle();
    });
});
 */
//////////////////////////////////////////////////
//CLIENT PORTAL 

// CALLBACK FUNCTIONS

function buttonToggle(){
    $('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    
    if ($(this).find('.btn-primary').size()>0) {
        $(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').size()>0) {
        $(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').size()>0) {
        $(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').size()>0) {
        $(this).find('.btn').toggleClass('btn-info');
    }
    
    $(this).find('.btn').toggleClass('btn-default');
       });

/*     $('form').submit(function(){
            alert($(this["options"]).val());
        return false;
    }); */
}

// multi step form 
function msForm(){
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $(".next").click(function(){
        
        if(animating) return false;
        animating = true;
        

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();



        
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        
        //show the next fieldset
        next_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale('+scale+')'});
                next_fs.css({'left': left, 'opacity': opacity});
            }, 
            duration: 800, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function(){
        if(animating) return false;
        animating = true;
        
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        
        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        
        //show the previous fieldset
        previous_fs.show(); 
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            }, 
            duration: 800, 
            complete: function(){
                current_fs.hide();
                animating = false;
            }, 
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".submit").click(function(){
        return false;
    });

}

// ON DOCUMENT READY

$(document).ready(function(){
    buttonToggle();
    msForm();
    //$('.client-rating').hide();
    $('client-discussion').hide();

    // JOB TITLE ATTENTION JS
    if ($('h2.job-title').is('.job-title-attn') === true){
        $('.job-title-attn').siblings('.job-notifications').hide();
        $('.job-title-attn').parent().append($('<a href="#"> <i class="fa fa-exclamation"> </i> </a>'));
        $('.client-discussion').hide();
    }


// mobile
var windowSize = $(window).width();
    if (windowSize < 970){
        $('.client-discussion').show();
        $('.client-rating').hide();
    }


    $(window).resize(function(){
        var width = $(document).width();
        if (width < 970) {
            $('.client-discussion').show();
            $('client-rating').hide();
        } else {
            $('.client-rating').hide();
            $('.client-discussion').hide();
        }
        console.log(width);
    });

    // click the view button under jobs to show details and conversation
    $('.view-job').on('click', function(){
        $('.create-job-post').toggle();
        $('.client-discussion').toggle();
        $(this).parent().toggleClass('active-job');
    });
    // when you click on red attention
    $('.job-title-attn').on('click', function(){
        $('.client-rating').show();

        $('.create-job-post').hide();
        $('.client-discussion').hide();

        $('.view-job').on('click', function(){
            $('.view-job').parent().toggleClass('active-job');
            $('.client-discussion').show();
            $('.create-job-post').hide();
            $('.client-rating').hide();
        });
    });

    // JOB NOTIFICATION CONTROLS
    $('.job-notifications').on('click', function(){
        $(this).siblings('.notification-controls').slideToggle();
    });


    // END OF JOB TITLE ATTENTION

});

//////////////////////////////////////////////////
//CHAT PAGE





