$(function(){
  $('.user')
    .hover(function(){
      $(this).children('.pointer').toggle();
      $(this).children('.user-info').toggle();
      });    
    $('.pop-over').popover({
      html: true,
      content: function() {
        return $('#popover_content_wrapper').html();
      }
    });

});